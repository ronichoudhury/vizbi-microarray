TAIR Accession = HybData:1005823620
Experiment Submission Number = 93
Experiment Name = CIRCADIAN RHYTHM
Experiment Description = Plants respond to day/night cycling in a number of physiological ways. At the mRNA level, the expression of some genes changes during the 24-hr period. To identify novel genes regulated in this way, we used microarrays containing 11,521 Arabidopsis expressed sequence tags, representing an estimated 7800 unique genes, to determine gene expression levels at 6-hr intervals throughout the day. Eleven percent of the genes, encompassing genes expressed at both high and low levels, showed a diurnal expression pattern. Approximately 2% cycled with a circadian rhythm. By clustering Microarray data from 47 additional nonrelated experiments, we identified groups of genes regulated only by the circadian clock. These groups contained the already characterized clock-associated genes LHY, CCA1, and GI, suggesting that other key circadian clock genes might be found within these clusters.
Experimenter = Robert Schaffer, rschaffer@hortresearch.co.nz
Slide Name = R12
Slide Description = circadian rhythm time = 12,0 (2)
AFGC exptid = 2365
Array Design = MSU-1
Samples:
	Name = 2365_2359_Cy3; Description = time = 12 hrs; Tissue = whole plant; Development stage = visible flower bud; Channel number = 1; Label = Cy3; test
	Name = 77-0hr_Cy5; Description = Time = 0 hrs; Tissue = whole plant; Development stage = visible flower bud; Channel number = 2; Label = Cy5; control
Scanning Software & version = ScanAlyze 2.33      

File containing the original normalized data = R12_original.txt
Original normalization method = global
Original normalization factor = 1.5
ANOVA sector for the original normalized data = NA
ANOVA plate for the original normalized data = 0.119

File containing TAIR-normalized data = R12_tair.txt
TAIR normalization method = print-tip-group lowess
TAIR normalization factor = 20% width of the lowess smooth
ANOVA sector for TAIR-normalized data = NA
ANOVA plate for TAIR-normalized data = 0.0010

Column definitions for the original normalized data in file R12_original.txt:
ARRAY_ELEMENT	Name of the element on an array as given by the manufacturer.
LOCUS	Mapped element that corresponds to a transcribed region in Arabidopsis genome, or a genetic locus that segregates as a single genetic locus or quantitative trait.
SPOT	Uniquely identifies feature/spot in array layout.
SUID	SMD unique identifier for a SMD sequence. 
CH1I_MEAN	Mean spot pixel intensity at Channel 1 (usually 532 nm).
CH1D_MEDIAN	Median spot pixel intensity at Channel 1 with the median-background subtracted (CH1I_MEDIAN - CH1B_MEDIAN).
CH1I_MEDIAN	Median spot pixel intensity at Channel 1 (usually 532 nm).
CH1_PER_SAT	Percentage of spot pixels at Channel 1 (usually 532 nm) that are saturated.
CH1I_SD	Standard deviation of the spot intensity at Channel 1 (usually 532 nm).
CH1B_MEAN	Mean spot background in Channel 1 (usually 532 nm).
CH1B_MEDIAN	Median spot background intensity in Channel 1 (usually 532 nm).
CH1B_SD	Standard deviation of the spot background intensity in Channel 1 (usually 532 nm).
CH1D_MEAN	Mean Channel 1 (usually 532 nm) intensity with the median-background subtracted (CH1I_MEAN - CH1B_MEDIAN)
CH2I_MEAN	Mean spot pixel intensity at Channel 2 (usually 635 nm).
CH2D_MEAN	Mean spot pixel intensity at Channel 2 (usually 635 nm) with the median-background subtracted (CH2I_MEAN - CH2B_MEDIAN).
CH2D_MEDIAN	Median spot pixel intensity at Channel 2 (usually 635 nm) with the median-background subtracted (CH2I_MEDIAN - CH2B_MEDIAN).
CH2I_MEDIAN	Median spot pixel intensity at Channel 2 (usually 635 nm).
CH2_PER_SAT	Percentage of spot pixels at Channel 2 (usually 635 nm) that are saturated.
CH2I_SD	Standard deviation of the spot pixel intensity at at Channel 2 (usually 635 nm).
CH2B_MEAN	Mean spot background intensity in Channel 2 (usually 635 nm).
CH2B_MEDIAN	Median spot background intensity in Channel 2 (usually 635 nm).
CH2B_SD	Standard deviation of the spot background intensity in Channel 2 (usually 635 nm).
CH2BN_MEDIAN	Normalized value of median Channel 2 (usually 635 nm) background (CH2B_MEDIAN/Normalization factor).
CH2DN_MEAN	Normalized value of mean Channel 2 (usually 635 nm) intensity with normalized background subtracted (CH2IN_MEAN - CH2BN_MEDIAN).
CH2IN_MEAN	Normalized value of mean Channel 2 (usually 635 nm) intensity (CH2I_MEAN/Normalization factor).
CH2DN_MEDIAN	Normalized value of median Channel 2 (usually 635 nm) intensity with normalized background subtracted (CH2IN_MEDIAN - CH2BN_MEDIAN).
CH2IN_MEDIAN	Normalized value of median Channel 2 (usually 635 nm) intensity (CH2_MEDIAN/Normalization factor).
CORR	ScanAlyze: correlation of Ch1 and Ch2 pixels in a spot. GenePix data: Sqrt(Coefficient of determination for the current regression value) with sign inferred from sign of REGR column. Value of '-1' indicates GenePix error in this field.
DIAMETER	Diameter in 5m of the spot indicator.
FLAG	Type of flag associated with a spot; set by either image processing software or manually; 0 - good, other than 0 - bad.
FAILED	Type of flag associated with the DNA used for printing a spot; set by verifying presence of a single PCR product on an agarose gel; 0 - good, other than 0 - bad.
LOG_RAT2N_MEAN	Log (base 2) of the ratio of the mean of Channel 2 (usually 635 nm) to Channel 1 (usually 532 nm) [log (base 2) (RAT2N_MEAN)].
LOG_RAT2N_MEDIAN	Log (base 2) of the ratio of the medians of Channel 2 (usually 635 nm) to Channel 1 (usually 532 nm) [log (base 2) (RAT2N_MEDIAN)].
PIX_RAT2_MEAN	Geometric mean of the pixel-by-pixel ratios of Channel 2 (usually 635 nm) to Channel 1 (usually 532 nm) pixel intensities, with the median background subtracted.
PIX_RAT2_MEDIAN	Median of pixel by-pixel ratios of Channel 2 (usually 635 nm) to Channel 1 (usually 532 nm) pixel intensities, with the median background subtracted.
PERGTBCH1I_1SD	Percentage of spot pixels with intensities more than one standard deviation above the background pixel intensity, at Channel 1 (usually 532 nm).
PERGTBCH1I_2SD	Percentage of spot pixels with intensities more than two standard deviations above the background pixel intensity, at Channel 1 (usually 532 nm).
PERGTBCH2I_1SD	Percentage of spot pixels with intensities more than one standard deviation above the background pixel intensity, at Channel 2 (usually 635 nm).
PERGTBCH2I_2SD	Percentage of spot pixels with intensities more than two standard deviations above the background pixel intensity, at Channel 2 (usually 635 nm).
RAT1_MEAN	Ratio of the arithmetic mean intensities of each spot for each wavelength, with the median background subtracted. Channel 1/Channel 2 ratio, (CH1I_MEAN - CH1B_MEDIAN)/(CH2I_MEAN - CH2B_MEDIAN) or Green/Red ratio.
RAT1N_MEAN	Ratio of the means of Channel 1 (usually 532 nm) intensity to normalized Channel 2 (usually 635 nm) intensity with median background subtracted (CH1D_MEAN/CH2DN_MEAN). Channel 1/Channel 2 ratio normalized or Green/Red ratio normalized.
RAT2_MEAN	Ratio of the arithmetic mean intensities of each spot for each wavelength, with the median background subtracted. Channel 2/Channel 1 ratio, (CH2I_MEAN - CH2B_MEDIAN)/(CH1_MEAN - CH1B_MEDIAN) or Red/Green ratio.
RAT2_MEDIAN	Ratio of the median intensities of each spot for each wavelength, with the median background subtracted.
RAT2_SD	Standard deviation of pixel intensity ratios.
RAT2N_MEAN	Channel 2/Channel 1 ratio normalized, RAT2_MEAN/Normalization factor or Red/Green mean ratio normalized.
RAT2N_MEDIAN	Channel 2/Channel 1 ratio normalized, RAT2_MEDIAN/Normalization factor or Red/Green median ratio normalized.
REGR	Regression ratio.
SUM_MEAN	Sum of the arithmetic mean intensities for each wavelength, with the median background subtracted (CH1D_MEAN + CH2D_MEAN).
SUM_MEDIAN	Sum of the median intensities for each wavelength, with the median-background subtracted (CH1D_MEDIAN + CH2D_MEDIAN).
TOT_BPIX	Total number of background pixels.
TOT_SPIX	Total number of spot pixels.
X_COORD	X-coordinate in 5m of the center of the spot-indicator associated with the spot, where (0,0) is the top left of the image.
Y_COORD	Y-coordinate in 5m of the center of the spot-indicator associated with the spot, where (0,0) is the top left of the image.
TOP	Box top: int(((centerX - radius) - Xoffset) / pixelSize).
BOT	Box bottom: int(((centerX + radius) - Xoffset) / pixelSize).
LEFT	Box left: int(((centerY - radius) - yoffset) / pixelSize).
RIGHT	Box right: int(((centerY + radius) - yoffset) / pixelSize)

Column definitions for TAIR-normalized data in file R12_tair.txt:
ARRAY_ELEMENT	Name of the element on an array as given by the manufacturer.
LOCUS	Mapped element that corresponds to a transcribed region in Arabidopsis genome, or a genetic locus that segregates as a single genetic locus or quantitative trait.
SUID	SMD unique identifier for a SMD sequence. 
FLAG	Type of flag associated with an array element; set by either image processing software or manually; 0 - good, other than 0 - bad.
IS_EXPRESSED	Qualitative measure of gene expression as a function of signal intensity; 'yes' - if intensity values >= 350 in both channels after background subtraction; 'no' - if intensity values < 350 in any channel after background subtraction; 'absent' - if the data were flagged as unreliable either by software or manually.
FOLD_CHANGE	How many times the expression signal for a given transcript increased or decreased compared to the control.	
STD_ERR	The standard deviation of the sampling distribution of the mean.
AVG_FOLD_CHANGE	Averaged fold change for all replicate hybridizations.	
AVG_STD_ERR	The standard deviation of the sampling distribution of the mean for the replicate set.

Questions? Please email to curator@arabidopsis.org.

